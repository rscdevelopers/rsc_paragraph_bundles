<?php

/**
 * @file
 * rsc_paragraph_bundles.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rsc_paragraph_bundles_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-bibliography-rscpb_epilogue'.
  $field_instances['paragraphs_item-bibliography-rscpb_epilogue'] = array(
    'bundle' => 'bibliography',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Content after the last list item.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'rscpb_epilogue',
    'label' => 'Epilogue',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'paragraphs_item-bibliography-rscpb_list_items'.
  $field_instances['paragraphs_item-bibliography-rscpb_list_items'] = array(
    'bundle' => 'bibliography',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'rscpb_list_items',
    'label' => 'List items',
    'required' => 1,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'paragraphs_item-bibliography-rscpb_preamble'.
  $field_instances['paragraphs_item-bibliography-rscpb_preamble'] = array(
    'bundle' => 'bibliography',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Content between the title and the first list item.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'rscpb_preamble',
    'label' => 'Preamble',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'paragraphs_item-bibliography-rscpb_title'.
  $field_instances['paragraphs_item-bibliography-rscpb_title'] = array(
    'bundle' => 'bibliography',
    'default_value' => array(
      0 => array(
        'value' => 'Bibliography',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'rscpb_title',
    'label' => 'Title',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Content after the last list item.');
  t('Content between the title and the first list item.');
  t('Epilogue');
  t('List items');
  t('Preamble');
  t('Title');

  return $field_instances;
}
