<?php

/**
 * @file
 * rsc_paragraph_bundles.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function rsc_paragraph_bundles_filter_default_formats() {
  $formats = array();

  // Exported format: Inline HTML.
  $formats['rscpb_inline_html'] = array(
    'format' => 'rscpb_inline_html',
    'name' => 'Inline HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_html' => array(
        'weight' => -10,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
