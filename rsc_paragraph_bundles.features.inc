<?php

/**
 * @file
 * rsc_paragraph_bundles.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function rsc_paragraph_bundles_paragraphs_info() {
  $items = array(
    'bibliography' => array(
      'name' => 'Bibliography',
      'bundle' => 'bibliography',
      'locked' => '1',
    ),
  );
  return $items;
}
