<ul class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php foreach ($items as $delta => $item): ?>
    <li class="field-item"<?php print $item_attributes[$delta]; ?>><?php
      print render($item);
    ?></li>
  <?php endforeach; ?>
</ul>
